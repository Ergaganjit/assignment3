import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
public class AirRideLimo extends Application{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		launch (args);

	}

	@Override
	public void start(Stage arg0) throws Exception {
		// TODO Auto-generated method stub
		
		Label heading = new Label("Airport Ride Calculator");
		Label headingFrom = new Label("From: ");
		//TextField textBoxFrom = new TextField();
		RadioButton radioButton1=new RadioButton("Cestar College");
		RadioButton radioButton2=new RadioButton("Brampton");
		
		CheckBox checkBox1=new CheckBox("Extra luggage?");
		CheckBox checkBox2=new CheckBox("Pets");
		CheckBox checkBox3=new CheckBox("Use 407 ETR?");
		CheckBox checkBox4=new CheckBox("Add Tip?");
		Button calculateButton = new Button();
		calculateButton.setText("CALCULATE");
		TextField result = new TextField();
		
		
		
		calculateButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override
		    public void handle(ActionEvent e) {
		    	
		            double baseFare=0;
		            double kilometer=0;
		            double additionCharges=0;
		    		//String textFrom =textBoxFrom.getText();
		    		
		    		if( radioButton1.isSelected())
		    		{
		    			baseFare=51;
		    			kilometer=7;
		    			
		    		}
		    		else if(radioButton2.isSelected())
		    		{
		    			baseFare=38;
		    			kilometer=10;
		    		}
		    		
		    		
		    		if(checkBox1.isSelected())
		    		{
		    			additionCharges=additionCharges+10;
		    			
		    		}
		    		 if (checkBox2.isSelected())
		    		{
		    			additionCharges=additionCharges+6;
		    		}
		    		 if (checkBox3.isSelected())
			    		{
			    			additionCharges=additionCharges+(0.25*kilometer);
			    		}
		    		double fare=baseFare+additionCharges;
		    		double tax=fare*0.13;
		    		double total=fare+tax;
		    		double tip=0.00;
		    		if(checkBox4.isSelected())
		    		{
		    			 tip=total*0.15;
		    		}
		    		double totalPrice=total+tip;
		    		
		    		
		    		result.setText("The total fare is : " + totalPrice);
		    		
		    }
		});
		
		
		VBox root = new VBox();
		root.setSpacing(10);	
		
		HBox root1 = new HBox();
		root.setSpacing(10);
		
		root.getChildren().add(heading);
		root.getChildren().add(headingFrom);
	//	root.getChildren().add(textBoxFrom);
		root.getChildren().add(radioButton1);
		root.getChildren().add(radioButton2);
		root.getChildren().add(checkBox1);
		root.getChildren().add(checkBox2);
		root.getChildren().add(checkBox3);
		root.getChildren().add(checkBox4);
		root.getChildren().add(calculateButton);
		root.getChildren().add(result);
		
	
		
		
		arg0.setScene(new Scene(root, 250, 300));
	
	//	arg0.setScene(new Scene(root1, 50, 50));
		
		arg0.setTitle("AirRideLimo Calculator");
		
		arg0.show();
		
	}

}
